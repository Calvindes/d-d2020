import { Injectable, ViewChild, ElementRef } from '@angular/core';
import { CharacterVariables } from "./CharacterVariables";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private charVar: CharacterVariables, protected http: HttpClient) { }

  //Get races in Character data
  public getRaces(route) {
    this.http.get(route).subscribe(response => {
            let data = response;
            this.charVar.races = data['results'];
        }, err => {
            throw err;
        });
  }

  //Get the information of the selected race
  public getRaceInfo(route) {

    return new Promise((resolve, reject) => {
          this.http.get(route).subscribe(response => {

              this.charVar.raceInfo = response;
              
              this.charVar.raceBonuses = this.charVar.raceInfo.ability_bonuses;
              this.charVar.raceAge = this.charVar.raceInfo.age;
              this.charVar.raceAlignment = this.charVar.raceInfo.alignment;
              this.charVar.raceLangDesc = this.charVar.raceInfo.language_desc;
              this.charVar.raceLangs = this.charVar.raceInfo.languages;
              this.charVar.raceSize = this.charVar.raceInfo.size;
              this.charVar.raceSizeDesc = this.charVar.raceInfo.size_description;
              this.charVar.raceSpeed = this.charVar.raceInfo.speed;
              this.charVar.raceProfics = this.charVar.raceInfo.starting_proficiencies;
              this.charVar.raceProficsOpts = this.charVar.raceInfo.starting_proficiency_options;
              this.charVar.raceSubraces = this.charVar.raceInfo.subraces;
              this.charVar.raceTraits = this.charVar.raceInfo.traits;
              this.charVar.raceLoaded = true;

              if(this.charVar.raceInfo.subraces.length > 0){
                this.charVar.haveSubraces = true;
              }else{
                this.charVar.haveSubraces = false;
              }

              setTimeout(function(){
                resolve();
              },1000);

          }, err => {
              reject(err);
          });
        }
    );
  }

  //Get the information of the selected subrace
  public getSubraceInfo(route) {

    return new Promise((resolve, reject) => {
          this.http.get(route).subscribe(response => {

              this.charVar.subraceInfo = response;

              this.charVar.subraceBonuses = this.charVar.subraceInfo.ability_bonuses;
              this.charVar.subraceLangs = this.charVar.subraceInfo.languages;
              this.charVar.subraceProfics = this.charVar.subraceInfo.starting_proficiencies;
              this.charVar.subraceProficsOpts = this.charVar.subraceInfo.starting_proficiency_options;
              this.charVar.subraceTraits = this.charVar.subraceInfo.traits;
              this.charVar.subraceLoaded = true;

              setTimeout(function(){
                resolve();
              },1000);

          }, err => {
              reject(err);
          });
        }
    );
  }
}
