import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'AppIndex',
  templateUrl: './app.AppIndex.html',
  styleUrls: [ './app.AppIndex.sass' ]
})
export class AppIndexComponent {
  title = 'index';
}
