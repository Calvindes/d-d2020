import { Component, ViewChild, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterVariables } from "../CharacterVariables";
import { HttpService } from "../httpGet.service";

@Component({
  selector: 'AppCharacterData',
  templateUrl: './app.AppCharacterData.html',
  styleUrls: [ './app.AppCharacterData.sass' ]
})

export class AppCharacterDataComponent {
  title = 'Character Data page';
  charVar: CharacterVariables;

  constructor(charVar: CharacterVariables, protected http: HttpService) {
    this.charVar = charVar;
  }

  @ViewChild('infoArea') textArea:ElementRef;

  ngOnInit() {
    this.http.getRaces('http://www.dnd5eapi.co/api/races');
  }

  public changeName() {
    let t = (<HTMLInputElement>event.target);
    this.charVar.name = t.value;
  }

  public changeSurname() {
    let t = (<HTMLInputElement>event.target);
    this.charVar.surname = t.value;
  }

  public changeGender(gender) {
    this.charVar.gender = gender;
  }

  public raceSelect(name, route) {
    this.charVar.race = name;
    this.http.getRaceInfo('https://www.dnd5eapi.co'+route);

    this.charVar.subraceInfo = [];
    this.charVar.subraceBonuses = [];
    this.charVar.subraceLangs = [];
    this.charVar.subraceProfics = [];
    this.charVar.subraceProficsOpts = [];
    this.charVar.subraceTraits = [];
    this.charVar.subraceLoaded = false;
  }

  public subraceSelect(name, route) {

    if(name == null){
      this.charVar.subraceInfo = [];
      this.charVar.subraceBonuses = [];
      this.charVar.subraceLangs = [];
      this.charVar.subraceProfics = [];
      this.charVar.subraceProficsOpts = [];
      this.charVar.subraceTraits = [];
      this.charVar.subraceLoaded = false;
    }else{
      this.charVar.subrace = name;
      this.http.getSubraceInfo('https://www.dnd5eapi.co'+route);
    }

  }

  public rollAbilities(){

    let abilitiesArray = new Array();

    for (let i = 0; i < 6; i++) {
      var rolls = new Array();
      for (let a = 0; a < 4; a++) {
        let roll = Math.round(Math.random() * (6 - 1) +1);
        rolls.push(roll);
      }
      let minIndex = rolls.indexOf(Math.min.apply(Math, rolls));
      rolls.splice(minIndex, 1);
      console.log(rolls);
    }

  }

  public setDefaultPic() {
    let t = (<HTMLInputElement>event.target);
    t.src = "assets/images/DefaultAvatar.png";
  }
}
