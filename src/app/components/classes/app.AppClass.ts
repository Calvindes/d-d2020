import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'AppClass',
  templateUrl: './app.AppClass.html',
  styleUrls: [ './app.AppClass.sass' ]
})
export class AppClassComponent {
  title = 'Class Data page';
}
