import { Component } from '@angular/core';
import { slideInAnimation } from './appRouterAnimations';

@Component({
  selector: 'AppHome',
  templateUrl: './app.AppHome.html',
  styleUrls: [ './app.AppHome.sass' ],
  animations: [ slideInAnimation ]
})
export class AppHomeComponent {
  title = 'Home page';

  prepareRoute(outlet) {
   return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
