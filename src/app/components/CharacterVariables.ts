import { Injectable } from '@angular/core';

@Injectable()
export class CharacterVariables {

  name: string = '';
  surname: string = '';

  gender: string = '';

  /****** Race Variables *******/

  races: [] = [];
  race: string = '';
  raceInfo: any = [];
  raceBonuses: any = [];
  raceAge: string = '';
  raceAlignment: string = '';
  raceLangDesc: string = '';
  raceLangs: any = [];
  raceSize: string = '';
  raceSizeDesc: string = '';
  raceSpeed: number = 0;
  raceProfics: any = [];
  raceProficsOpts: any = [];
  raceSubraces: any = [];
  raceTraits: any = [];
  haveSubraces: boolean = false;
  raceLoaded: boolean = false;

  /****** Subace Variables *******/

  subrace: string = '';
  subraceInfo: any = [];
  subraceBonuses: any = [];
  subraceLangs: any = [];
  subraceProfics: any = [];
  subraceProficsOpts: any = [];
  subraceTraits: any = [];
  subraceLoaded: boolean = false;
}
