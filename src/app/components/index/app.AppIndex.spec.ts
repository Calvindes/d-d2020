import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppIndexComponent } from './app.AppIndex';

describe('AppToolbarComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppHomeComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppIndexComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'DD2020'`, () => {
    const fixture = TestBed.createComponent(AppIndexComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('DD2020');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppIndexComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('DD2020 app is running!');
  });
});
