import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CharacterVariables } from './components/CharacterVariables'
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppHomeComponent } from './components/home/app.AppHome';
import { AppToolbarComponent } from './components/toolbar/app.AppToolbar';
import { AppPanelComponent } from './components/panel/app.AppPanel';
import { AppIndexComponent } from './components/index/app.AppIndex';
import { AppCharacterDataComponent } from './components/characterData/app.AppCharacterData';
import { AppClassComponent } from './components/classes/app.AppClass';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpService } from './components/httpGet.service';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppHomeComponent,
    AppToolbarComponent,
    AppPanelComponent,
    AppCharacterDataComponent,
    AppClassComponent,
    AppIndexComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FontAwesomeModule,
    NgbModule,
    CommonModule,
    HttpClientModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule
  ],
  providers: [CharacterVariables, HttpService],
  bootstrap: [AppHomeComponent, AppToolbarComponent, AppPanelComponent],
  exports: [ CommonModule ]
})

export class AppModule { }
