import { Component } from '@angular/core';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'AppToolbar',
  templateUrl: './app.AppToolbar.html',
  styleUrls: [ './app.AppToolbar.sass' ]
})
export class AppToolbarComponent {
  title = 'Toolbar';
  faArrowRight = faArrowRight;
}
