import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'AppPanel',
  templateUrl: './app.AppPanel.html',
  styleUrls: [ './app.AppPanel.sass' ]
})
export class AppPanelComponent {
  title = 'Panel';
}
