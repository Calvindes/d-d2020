import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppHomeComponent } from './components/home/app.AppHome';
import { AppCharacterDataComponent } from './components/characterData/app.AppCharacterData';
import { AppClassComponent } from './components/classes/app.AppClass';
import { AppIndexComponent } from './components/index/app.AppIndex';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/index', data: {animation: 'index'}},
  { path: 'index', component: AppIndexComponent, data: {animation: 'characterData'}},
  { path: 'characterData', component: AppCharacterDataComponent, data: {animation: 'characterData'}},
  { path: 'class', component: AppClassComponent, data: {animation: 'class'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
